// node progress.js dave3 'WzTNnM8HOoU YLJiBGZl9-0 sdp-EfgrP9Y'
// node progress.js dave WzTNnM8HOoU

const readline = require('readline');
const path     = require('path');
const fs       = require('fs');
const ytdl     = require('ytdl-core');


var args = process.argv.slice(2);

var folder = args[0];
var ids = args[1].split(" ");

for (var id of ids) {
    downloadbyid(id);
}

function downloadbyid(id) {

  if (!fs.existsSync(path.resolve(__dirname + '/descargas/' + folder + '/'))) {
      fs.mkdirSync(path.resolve(__dirname + '/descargas/' + folder + '/'));
  }

  ytdl.getInfo(id, (err, info) => {
    console.log(id);
    if (err) throw err;

    const output = path.resolve(__dirname + '/descargas/' + folder + '/', info.title.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_') + '.mp4');
    console.log("Titulo: " + output + ' by ' + info.author.name);
    const url = 'https://www.youtube.com/watch?v=' + id;
    const video = ytdl(url);

    let starttime;
    video.pipe(fs.createWriteStream(output));
    video.once('response', () => {
      starttime = Date.now();
    });
    video.on('progress', (chunkLength, downloaded, total) => {
      const floatDownloaded = downloaded / total;
      const downloadedMinutes = (Date.now() - starttime) / 1000 / 60;
      readline.cursorTo(process.stdout, 0);
      process.stdout.write(`${(floatDownloaded * 100).toFixed(2)}% downloaded`);
      process.stdout.write(`(${(downloaded / 1024 / 1024).toFixed(2)}MB of ${(total / 1024 / 1024).toFixed(2)}MB)\n`);
      process.stdout.write(`running for: ${downloadedMinutes.toFixed(2)}minutes`);
      process.stdout.write(`, estimated time left: ${(downloadedMinutes / floatDownloaded - downloadedMinutes).toFixed(2)}minutes `);
      readline.moveCursor(process.stdout, 0, -1);
    });
    video.on('end', () => {
      process.stdout.write('\n\n');
    });

  });

}
