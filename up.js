// node up.js private 12
// node up.js private 0 Wz5pusVmAy4
// node up.js private 0 3gql0qjhRL4 pajarotintin

var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var util = require('util');
const path     = require('path');
const ytdl     = require('ytdl-core');

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/youtube-nodejs-quickstart.json

var args = process.argv.slice(2);
var VISIBLE = args[0];
var LIMIT = args[1];
var VIDEO_ID = args[2];
var AUTENTICACION = args[3];

console.log("AUTENTICACION", AUTENTICACION);
var SCOPES = [
  'https://www.googleapis.com/auth/youtube.upload',
  'https://www.googleapis.com/auth/youtube'
];
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';

var autenticacion = AUTENTICACION;
if (AUTENTICACION == undefined) {
    AUTENTICACION = Math.floor((Math.random() * 1000000) + 1);
}
console.log("AUTENTICACION", AUTENTICACION);

var TOKEN_PATH = TOKEN_DIR + 'youtube-nodejs-controltotal-' + autenticacion + '.json';

// Load client secrets from a local file.
fs.readFile('client_secret.json', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading client secret file: ' + err);
    return;
  }
  // Authorize a client with the loaded credentials, then call the YouTube API.
  authorize(JSON.parse(content), uploadVideo);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  var clientSecret = credentials.installed.client_secret;
  var clientId = credentials.installed.client_id;
  var redirectUrl = credentials.installed.redirect_uris[0];
  var auth = new googleAuth();
  var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, function(err, token) {
    if (err) {
      getNewToken(oauth2Client, callback);
    } else {
      oauth2Client.credentials = JSON.parse(token);
      callback(oauth2Client);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {
  var authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  console.log('Authorize this app by visiting this url: ', authUrl);
  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the code from that page here: ', function(code) {
    rl.close();
    oauth2Client.getToken(code, function(err, token) {
      if (err) {
        console.log('Error while trying to retrieve access token', err);
        return;
      }
      oauth2Client.credentials = token;
      storeToken(token);
      callback(oauth2Client);
    });
  });
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
  try {
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code != 'EEXIST') {
      throw err;
    }
  }
  fs.writeFile(TOKEN_PATH, JSON.stringify(token));
  console.log('Token stored to ' + TOKEN_PATH);
}

/**
 * Lists the names and IDs of up to 10 files.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function getChannel(auth) {
  var service = google.youtube('v3');
  service.channels.list({
    auth: auth,
    part: 'snippet,contentDetails,statistics',
    forUsername: 'GoogleDevelopers'
  }, function(err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    var channels = response.items;
    if (channels.length == 0) {
      console.log('No channel found.');
    } else {
      console.log('This channel\'s ID is %s. Its title is \'%s\', and ' +
                  'it has %s views.',
                  channels[0].id,
                  channels[0].snippet.title,
                  channels[0].statistics.viewCount);
    }
  });
}

function uploadVideo (auth) {

    const youtube = google.youtube({
      version: 'v3',
      auth: auth
    });

    if (VIDEO_ID != "") {
      downloadSpecificById(youtube,VIDEO_ID);
    }
    else {
      getMostPopular(youtube);
    }

}

function upFile(y,filePathName,title,tags) {

  var req = y.videos.insert({
    part: 'id,snippet,status',
    notifySubscribers: false,
    resource: {
      snippet: {
        title: title,
        description: '',
        tags: tags
      },
      status: {
        privacyStatus: VISIBLE
      }
    },
    media: {
      body: fs.createReadStream(filePathName)
    }
  }, function (err, data) {
    if (err) {
      console.error('Error: ' + err);
    }
    if (data) {
      console.log(util.inspect(data, false, null));
    }
    process.exit();
  });

  var fileSize = fs.statSync(filePathName).size;

  // show some progress
  var id = setInterval(function () {
    var uploadedBytes = req.req.connection._bytesDispatched;
    var uploadedMBytes = uploadedBytes / 1000000;
    var progress = uploadedBytes > fileSize
      ? 100 : (uploadedBytes / fileSize) * 100;
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(uploadedMBytes.toFixed(2) + ' MBs uploaded. ' +
     progress.toFixed(2) + '% completed.');
    if (progress === 100) {
      process.stdout.write('\nDone uploading, waiting for response...\n');
      clearInterval(id);
    }
  }, 250);

}

function downloadbyId(y,id,tags) {
	// AIzaSyAALzanM2JWnP7bqOmKDws78WOkQz2Tqpg
	// 1067423764027-utv3i9n13ccihlvgeph1rhs5tfbh5p0j.apps.googleusercontent.com

	// if (!fs.existsSync(path.resolve(__dirname + '/descargas/mostPopular/'))) {
	//     fs.mkdirSync(path.resolve(__dirname + '/descargas/mostPopular/'));
	// }

	ytdl.getInfo(id, (err, info) => {
	  if (err) throw err;
    console.log("info: ", info.title, info.view_count, info.length_seconds);
	  const output = path.resolve(__dirname + '/descargas/mostPopular/', info.title.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_') + ' by ' + info.author.name + '.mp4');

    if (!fs.existsSync(path.resolve(output)) && parseInt(info.length_seconds) < 300 && parseInt(info.view_count) > 10000) {
      console.log("info ESTE VA: ", info.title, info.view_count, info.length_seconds);
      const url = 'https://www.youtube.com/watch?v=' + id;
      const video = ytdl(url);

      let starttime;
      video.pipe(fs.createWriteStream(output));
      video.once('response', () => {
        starttime = Date.now();
      });
      video.on('progress', (chunkLength, downloaded, total) => {
        const floatDownloaded = downloaded / total;
        const downloadedMinutes = (Date.now() - starttime) / 1000 / 60;
        readline.cursorTo(process.stdout, 0);
        process.stdout.write(`${(floatDownloaded * 100).toFixed(2)}% downloaded`);
        process.stdout.write(`(${(downloaded / 1024 / 1024).toFixed(2)}MB of ${(total / 1024 / 1024).toFixed(2)}MB)\n`);
        process.stdout.write(`running for: ${downloadedMinutes.toFixed(2)}minutes`);
        process.stdout.write(`, estimated time left: ${(downloadedMinutes / floatDownloaded - downloadedMinutes).toFixed(2)}minutes `);
        readline.moveCursor(process.stdout, 0, -1);
      });
      video.on('end', () => {
        process.stdout.write('\n\n');
        console.log("DONE: ", info.title);
        upFile(y,output,info.title.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_') + ' by ' + info.author.name,tags);
      });

  	}

	});

}


function getMostPopular(y) {

  var YouTube = require('youtube-node');

  var youTube2 = new YouTube();
  youTube2.setKey('AIzaSyAALzanM2JWnP7bqOmKDws78WOkQz2Tqpg');

  youTube2.getMostPopular(LIMIT, function (error, result) {
      if (error) {
          console.log(error);
      }
      else {
          //console.log(JSON.stringify(result, null, 2));
          for(var item of result.items) {
            if (item.kind == "youtube#video") {
              downloadbyId(y,item.id,item.snippet.tags);
            }
          }
      }
  });

}

function downloadSpecificById(y,id) {

  ytdl.getInfo(id, (err, info) => {
    if (err) throw err;

    const output = path.resolve(__dirname + '/descargas/especifico/', info.title.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_') + ' by ' + info.author.name + '.mp4');
    console.log("Titulo: " + output);
    const url = 'https://www.youtube.com/watch?v=' + id;
    const video = ytdl(url);

    let starttime;
    video.pipe(fs.createWriteStream(output));
    video.once('response', () => {
      starttime = Date.now();
    });
    video.on('progress', (chunkLength, downloaded, total) => {
      const floatDownloaded = downloaded / total;
      const downloadedMinutes = (Date.now() - starttime) / 1000 / 60;
      readline.cursorTo(process.stdout, 0);
      process.stdout.write(`${(floatDownloaded * 100).toFixed(2)}% downloaded`);
      process.stdout.write(`(${(downloaded / 1024 / 1024).toFixed(2)}MB of ${(total / 1024 / 1024).toFixed(2)}MB)\n`);
      process.stdout.write(`running for: ${downloadedMinutes.toFixed(2)}minutes`);
      process.stdout.write(`, estimated time left: ${(downloadedMinutes / floatDownloaded - downloadedMinutes).toFixed(2)}minutes `);
      readline.moveCursor(process.stdout, 0, -1);
    });
    video.on('end', () => {
      process.stdout.write('\n\n');
      console.log("DONE: ", info.title);
      upFile(y,output,info.title.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_') + ' by ' + info.author.name,[]);
    });

  });

}
