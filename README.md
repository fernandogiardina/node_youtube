# Node Youtube Downloader

## Requerimientos
node 7 o superior

## Que hace?
Descarga videos de youtube en formato mp4

## Como funciona?
- Consta de dos archivos .js. Uno para descargar un video o lista de videos por su id de video y el otro js es para realizar una busqueda y descargar la lista encontrada
- Si la carpeta contenodora no existe la crea.


### progress.js
#### parametros: [nombre de la carpeta] [lista de id de videos especificos que se quiere descargar] | id de video unico que se quiere descargar
```
node progress.js folder1 'WzTNnM8HOoU YLJiBGZl9-0 sdp-EfgrP9Y'
node progress.js folder2 WzTNnM8HOoU
```

### progress2.js
#### parametros: [nombre de la carpeta] [cantidad de resultados] [busqueda]
```
node progress2.js elmono 10 'el mono vapeador'
```


